#!/usr/bin/env python
import json

import dns
import dns.message
import dns.name
import dns.query

from socket import gethostbyname as ghost
from socket import gaierror

class PreFlight:
    def __init__(self):
        self.domains = None
        self.a_records = {}
        self.ns_records = {}
        self.nameserver = '8.8.8.8'
        self.domainfilename = '/etc/userdomains'
        
    def __str__(self):
        """ should be the text output of the run, unless it has not run """
        return "PreFlight Check"

    def set_domainfilename(self, filename):
        self.domainfilename = filename
    
    def read_domains(self):
        """ 
        /etc/userdomains is formatted as domain: user
        read the domains, store them in the class, since we'll be working with them later
        """
        if not self.domains:
            domains = []
            for line in  open(self.domainfilename, 'r'):
                domains.append(line.split(':')[0].strip())
                self.domains = domains
        return self.domains

    def lookup_a_records(self):
        if not self.a_records:
            self.a_records = {}
            if not self.domains:
                self.read_domains()
            for domain in self.domains:
                try:
                    address = ghost(domain)
                except gaierror:
                    address = '0.0.0.0'
                    # print("\n%s dns lookup failed, recording 0\n" %(domain))
                self.a_records[domain]=address
        return self.a_records

    def ns_query_for(self, domain):
        request = dns.message.make_query(domain, dns.rdatatype.NS)
        response= dns.query.udp(request, self.nameserver)
        return response
        
    def get_ns_records(self):
        if not self.a_records:
            self.lookup_a_records()
        for domain in self.domains:
            if self.a_records[domain] != '0.0.0.0':
                answer = self.ns_query_for(domain).answer
                if answer == []:
                    self.ns_records[domain] = answer
                else:
                    self.ns_records[domain] = [str(ns) for ns in answer[0]] 
    
    def dump(self):
        if not self.a_records:
            self.lookup_a_records()
        return json.dumps({'domains': self.domains,
                           'nameservers': self.ns_records,
                           'ip addresses': self.a_records})
