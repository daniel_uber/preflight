#!/usr/bin/env python

import unittest
import json
from preflight import PreFlight

class TestPreFlightChecks(unittest.TestCase):
    def seeded(self):
        self.p.read_domains()
        return self.p

    def setUp(self):
        self.p = PreFlight()
        self.p.set_domainfilename('testfile')        
    
    def test_default_to_userdomains(self):
        p = PreFlight()
        self.assertEqual(p.domainfilename, '/etc/userdomains')
        
    def test_can_set_alternate_file(self):
        p = PreFlight()
        p.set_domainfilename('testfile')
        self.assertEqual(p.domainfilename, 'testfile')

    def test_reads_domains(self):
        self.assertTrue('example.com' in self.p.read_domains())

    def test_saves_domains(self):
        p = self.seeded()
        self.assertTrue('example.com' in p.domains)

    def test_uses_hosts_file(self):
        self.p.lookup_a_records()
        self.assertTrue( self.p.a_records['localhost']=='127.0.0.1' )

    def test_always_has_a_record_for_each_zone(self):
        self.p.lookup_a_records()
        self.assertEqual(len(self.p.a_records), len(self.p.domains))

    def test_dump_outputs_json(self):
        d = self.p.dump()
        l = json.loads(d)
        self.assertTrue(l['domains'] == self.p.domains)
        self.assertTrue(l['ip addresses'] == self.p.a_records)

    def test_invalid_has_no_nameservers(self):
        self.p.get_ns_records()
        self.assertRaises(KeyError, self.p.ns_records.__getitem__, 'unkown.invalid')

    def test_localhost_has_nameservers(self):
        self.p.get_ns_records()
        # localhost not 0.0.0.0, lookup succeeds from hosts file,
        # but google won't give NS records:
        self.assertEqual([],  self.p.ns_records['localhost'])

    def test_example_com_nameservers(self):
        self.p.get_ns_records()
        self.assertTrue('a.iana-servers.net.' in self.p.ns_records['example.com'][0:2])
        
if __name__ == '__main__':
    unittest.main()
