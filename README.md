# Preflight Checklist

# Goals
- before migration, record headers and dns information from each domain listed in userdomains
- serialize to json
- after migration, run postflight with the same... compare to make sure nothing broke
- expected variations: etags, expires raw date (time delta should be the same)

# Implementation 
- written in python 2.6 for maximum compatibility (/usr/local/wt_python/bin/python is installed on even centos 5 systems)

# Basic Algorithm
- read /etc/userdomains
- check that domain is valid (i.e. NS records exist at the tld nameservers)
- check if domain is hosted here (ips resolve to this server?). Record NS and A record.
- check http://domain for response code. If 3XX, record location, if location on domain, follow, record final header as well as location history.


# use:
start a virtualenv, or pip install dnspython in the global system.
```bash
pip -r requirements.txt
```

It doesn't work yet, so you would need to manually get it to run, but it will get better.